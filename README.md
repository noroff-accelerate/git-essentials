# Git Essentials - Collaborative Development with GitLab

This project contains hands-on activities and tasks for introduction to Git, including its integration and usage with GitLab.

The activities and tasks cover basics of version control and Git operations, such as repository initialisation, committing changes, and branch management; and more advanced activities related to collaborative development using GitLab, from setting up projects to handling merge requests and code review processes.

## Lesson 1
* [Task - Introduction to Version Control](./Lesson%201%20-%20Introduction%20to%20Version%20Control/Task%20-%20Introduction%20to%20Version%20Control.md)

## Lesson 2
* [Activity - Working with remote Repositories](./Lesson%202%20-%20Collaborating%20with%20remote%20repositories/Activity%20-%20Working%20with%20remote%20Repositories.md)
* [Activity - Implementing GitHub Flow on a Sample Project](./Lesson%202%20-%20Collaborating%20with%20remote%20repositories/Activity%20-%20Implementing%20GitHub%20Flow%20on%20a%20Sample%20Project.md)
* [Task - Collaborating with remote repositories](./Lesson%202%20-%20Collaborating%20with%20remote%20repositories/Task%20-%20Collaborating%20with%20remote%20repositories.md)

## Lesson 3
* [Final Assignment - Project Management on GitLab](./Lesson%203%20-%20Project%20Management%20on%20GitLab/Assignmet%20-%20Project%20Management%20on%20GitLab.md)

## License

This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
