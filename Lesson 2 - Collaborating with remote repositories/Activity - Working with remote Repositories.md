# Lesson 2 Activity - Working with Remote Repositories

Let's work together with a remote repository! 🚀

Make sure you have [Git installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and properly configured on your machine, and set up an [account on GitLab](https://gitlab.com/) if you haven't already. You will need developer access to [this sandbox repository](https://gitlab.com/noroff-accelerate/git-essentials-collab-sandbox). Ensure the instructor has granted you the appropriate access.

Now, let's introduce ourselves! Start by cloning the sandbox repository:

1. Open a terminal and navigate to the directory where you want to clone the repository

2. Clone the repository at `https://gitlab.com/noroff-accelerate/git-essentials-collab-sandbox`
   * Notice the *README* and *.gitignore* files in the repository, used for documentation and specifying ignored files and directories, respectively.

3. Using your favorite editor or IDE, create a new text file in the [`Introductions` folder](https://gitlab.com/noroff-accelerate/git-essentials-collab-sandbox/-/tree/main/Introductions) of the project

4. Make sure the file has a unique name, e.g., by using your full name or adding some random letters/numbers (we want to avoid any conflicts for now, those come later 😋)

5. Open the file and add a few interesting facts about yourself (**nothing sensitive!**)

6. At the end of the file, add two facts `Mood: Happy` and `Favorite vegetable: Aubergine`, each on a separate line:
    ```
    <your content>
    Mood: Happy
    Favorite vegetable: Aubergine
    ```

We'll use these in later activities 😉

6. Stage your changes using the appropriate command

7. Commit the changes with a message saying *"Added interesting facts about myself"*

8. Push your changes to the remote repository

9. In your browser, navigate to the remote repository and verify your changes

10. On your local machine, fetch the remote changes and see what others have added to the repository 🤝
