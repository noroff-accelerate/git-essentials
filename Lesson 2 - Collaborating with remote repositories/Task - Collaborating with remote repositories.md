# Lesson 2 Task - Advanced Collaboration and Branching in GitLab

In this task, we'll use the local Git repository we created in [Lesson 1 Task](../Lesson%201%20-%20Introduction%20to%20Version%20Control/Task%20-%20Introduction%20to%20Version%20Control.md). Only now we're inviting more people to work on our recipes and things might get a bit heated 🔥 Make sure you have completed the lesson 1 task before continuing with this one.

We'll build on the concepts of remote repositories using GitLab, and collaboration following the GitHub Flow branching strategy. Remember, in GitHub Flow, branch naming conventions are typically straightforward and focused on the feature or issue being worked on. We will follow this approach and avoid using prefixes like "hotfix" or "feature" for our branches to streamline our workflow.

You're tasked with creating and managing a GitLab repository with multiple contributors working on a recipe collection. Let's make some good food 🍴

1. Log in to GitLab and create a new **blank** and **private** project named **Recipes** (untick "Initialize repository with a README")
   * See ["How to Create a Project in GitLab"](../Resources/gitlab-project.md) for more details

2. In your **local Recipes** repository, in the root directroy, create a *README.md* file and add a title and short description of the project

3. Stage and commit the README with an appropriate message

4. Push your existing **local Recipes** repository to the newly created empty repository in the GitLab project (follow the instructions for "Push an existing Git repository")
   * 💡Make sure to set the proper repository setting for collaboration: from the left sidebar in the GitLab project, select “Settings”, then “Repository”, and under “Protected branches”, for the main branch, change the “Allowed to merge” to “Developers + Maintainers”.

5. Invite one of your classmates to collaborate with you on the repository (in the GitLab portal, see "Manage" > "Members")
   * 💡Make sure to assign the proper role “Developer” for the invitee.
    
    ```If you have no one to collaborate with, please notify the instructor!```

## We're ready to get to work!

Our restaurant needs more business, so let's get to it 🍲

Our salad is reported to be a little bland and doesn't sell. Unfortunately, since there is no proper workflow management yet, both of you end up fixing the bland salad recipe without any coordination.

1. One developer **only**: Create a branch from the main branch, named `salad-upgrade-1`.

2. Push the `salad-upgrade-1` branch to the remote **Recipes** repository on GitLab.

3. Ensure all developers have an updated view of the central repository.

4. Both of you switch to the `salad-upgrade-1` branch, and make changes to **salad.txt**, adding a new ingredient *Feta Cheese* at the end:
    ```
    ...
    Croutons
    Feta Cheese
    ```

5. Stage, commit, and push your changes to the `salad-upgrade-1` branch with a message saying *"Added ingredient: Feta Cheese"*.

⚠️ The developer who pushes last here will experience an error, the `git push` is `rejected`. Since the remote branch has already been updated, you need to perform a `git pull` first first to ensure you don't overwrite any changes in the remote repository by accident. In this case, a `pull` will cause no issue, since the changes are identical and Git will handle the merge without any issues.

*Make sure to push your changes once you've done a pull!* In this case, this won't change any of the actual content, but keep the history log consitent with all changes.

## Spice up the salad!

Since everyone involved is eager to impress, you now each add your own touch to the recipe.

1. At the end of the recipe, **one of you** add the ingredient *Red Onion*, and **the other** add *Black Olives*. **Agree among you who does which!**

2. Stage, commit, and push your changes to the `salad-upgrade-1` branch with a message saying *"Added ingredient: \<whichever ingredient you added\>"*

⚠️ Just as with the previous change, the developer who pushes last here will experience the same error. In this case however, the following `pull` will cause a **merge conflict**, since the changes are different!

* The conflict is on the last line of the recipe, with a conflict between *Red Onion* and *Black Olives*

## Fix the merge conflict!

Agree among you what the right ingredient should be. Consider actively collaborating on following steps, either by physically sitting next to each other (the best option!), or on video. For the developer who has the merge conflict:

1. Open the file containing the merge conflict (**salad.txt**) in your favorite IDE or text editor

2. Look for the conflict markers `<<<<<<<`, `=======`, and `>>>>>>>`
  * The lines between `<<<<<<< HEAD` and `=======` show **your** changes
  * The lines between `=======` and `>>>>>>>` represent the changes from the remote branch that are causing the conflict

3. Decide how you want to resolve the conflict: edit the file accordingly to keep the ingredient you agreed on

4. Once you've resolved the conflict (edited the file and saved), go back to the terminal and stage, commit, and push the changes to the `salad-upgrade-1` branch with a message saying *"Resolved merge conflict in salad recipe"*

5. This marks the resolution of the merge conflict, our salad looks a lot more tempting and we are ready to put in into production, meaning merging the updated recipe to our `main branch`.

6. Before merging the salad upgrade back to the `main branch`, it is recommended to merge "the other way" first, so merge the `main branch` with the `salad-upgrade-1` branch you are currently working on (this is to avoid potential conflicts in the `main branch`)

7. Switch to the `main branch` and merge it with the `salad-upgrade-1` branch

8. Remember to push the changes in the `main branch` after the merge

9. In your browser, navigate to the remote repository and verify your changes (both `main` and `salad-upgrade-1` branches are updated)

**Optional**: Repeat this workflow and change the order of who merges with main first, so that the one who experienced the merge conflict now merges first, so both of you can experience handling merge conflicts.

## Now on to our new dishes 🥪

We also need a few more dishes, and this time, having learned from our previous merge-conflicted salad upgrade, each developer creates a separate branch to work on. Agree who does which and work separately on each new recipe, **pasta** and **BLT**:

1. One developer: Create a branch from the main branch and name it `pasta-recipe`

2. The other developer: Create another branch from the main branch and name it `blt-recipe`

3. Push your new branch to the remote **Recipes** repository on GitLab

4. Ensure all developers have an updated view of the central repository, that is, everyone has fetched information on the latest changes made - this way you can see what others are doing and maybe find inspiration 🎻

5. In the `pasta-recipe` branch, create a new file called **pasta.txt** and add a few ingredients:
    ```
    Spaghetti
    Tomato Sauce
    Ground Beef
    Garlic
    Basil
    Parmesan Cheese
    ```

6. Similarly, in the `blt-recipe` branch, create a new file called **blt.txt** and add a few ingredients:
    ```
    Bacon
    Lettuce
    Tomato
    Whole Wheat Bread
    Mayonnaise
    Avocado
    ```

7. Stage, commit, and push each new recipe to its respective branch with a message saying *"Added new recipe: \<name of your new dish\>"*

8. Check the status of your branch: verify there are no pending changes ("nothing to commit, working tree clean") and 1 log entry reflecting the newly added recipe

9. In your browser, navigate to the remote repository and verify your branch is updated accordingly (new recipe file added)

At any time after this, you can check out the other branch, that is, the other new recipe you are **not** working on and pull the changes to see what your colleague is working on (assuming your colleague has pushed their changes). You can also view this in the browser.

You can now make your new recipes available in the production line, meaning merge it into our `main branch`.

10. Just like with our salad upgrade, before merging the new recipes back into the `main branch`, merge the `main branch` into each new recipe branch, i.e., the one you are currently working on

11. Switch back to the `main branch`, pull the latest changes and merge it with your recipe branch

⚠️ While it's always good practice to pull the latest changes before merging, Git will still complete the merge automatically. This is because Git can safely integrate changes from branches where there is no overlap or conflict.

12. In your browser, navigate to the remote repository and verify your changes (both main and recipe branches are updated)

This scenario exemplifies how using separate branches for different tasks in a collaborative Git environment helps in avoiding merge conflicts. It underscores the importance of branch management in maintaining an organized and conflict-free workflow. Additionally, it highlights the possibilities for sharing up-to-date changes among developers to coordinate efforts effectively.