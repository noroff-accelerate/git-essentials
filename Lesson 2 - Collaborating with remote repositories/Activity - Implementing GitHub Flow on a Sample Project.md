# Lesson 2 Activity - Implementing GitHub Flow on a Sample Project

Let's practice [GitHub Flow](https://docs.github.com/en/get-started/using-github/github-flow) and manage a merge conflict. Remember, the main purpose of this flow is to keep our `main branch` production ready 🌟. In GitHub Flow, branches are named based on the feature or issue they address, typically without prefixes like "hotfix" or "release".

We'll be working on the [sandbox repository](https://gitlab.com/noroff-accelerate/git-essentials-collab-sandbox) and **our own introductions** from the [previous activity](./Activity%20-%20Working%20with%20remote%20Repositories.md). Make sure you have completed the previous activity before continuing with this one.

## Set it all up!

To start, if you haven't already, clone the sandbox repository:

1. Open a terminal and navigate to the directory where you want to clone the repository

2. Clone the repository at `https://gitlab.com/noroff-accelerate/git-essentials-collab-sandbox`

3. Ensure you are working on the `main` branch

4. We want to add additional information to our introduction using the GitHub Flow strategy. Create a branch from the `main branch` for this, ensuring the branch has a descriptive and unique name (e.g., `<your-name>-more-facts`)

5. It appears that our introduction also contains a critical error 🤦 (no one really likes aubergine). Create another branch, this time from the `main branch`, ensuring the branch has a descriptive and unique name (e.g., `<your-name>-error-fix`)

6. Verify that you have two new branches locally, and push both to the remote repository

7. In your browser, navigate to the remote repository and verify your changes (two new branches added)

## Great! Let's get to work! 👷🏻

We start by fixing our critical error, as this is always a priority.

1. Switch to the branch for fixing the error (`<your-name>-error-fix`)

2. Change the last line `Favorite vegetable` in **your own introduction file** to your actual favorite (obviously not 🍆)

3. While on this branch, also change your `Mood` to `Upset`, since having to do fixes isn't our cup of tea 🥵

4. Stage, commit, and push the changes to the branch with a message saying *"Fixed error: correct favorite vegetable"*

5. Before merging the fix back to the `main branch`, it is recommended to merge "the other way" first, so merge the `main branch` with the branch you are currently working on (this is to avoid potential conflicts in the `main branch`)

6. Switch back to the `main branch`, pull the latest changes and merge it with the branch for fixing the error - we've fixed our critical error, and no one will now mistakenly think we like aubergine 😅

7. In your browser, navigate to the remote repository and verify your changes (both `main` and the `error-fix` branches are updated)

## Excellent! But we're not done!

Let's add a new fact to our introduction.

1. Switch to the branch for adding a new fact (`<your-name>-more-facts`)

2. At the end of **your own introduction file**, add a new fact of your own choice on a separate line:
    ```
    <your content>
    Mood: Happy
    Favorite vegetable: Aubergine
    <a new fact about you>
    ```

3. Also change your `Mood` to `Excited`, since sharing more facts about yourself is indeed exciting 🤠

4. Stage, commit, and push the changes to the branch for adding a new fact with a message saying *"Added new fact about \<your name\>"*

5. Just like with our previous fix, before merging the new fact back into the `main branch`, merge the `main branch` into the branch you are currently working on

6. Alas!!! We encounter a **merge conflict** 😨, seeing as the file we've altered in our branch for adding a new fact has been modified in the same place in the `main branch` since after we created the branch

If you've followed the activities up to this point, you will notice that **only** the line where changes occurred in both branches is marked as a merge conflict:

* The conflict is on the `Mood` line. The change from the `main branch` shows "Upset", while your current change in the branch for adding a new fact indicates "Excited"
* The line with your `Favorite vegetable` has been updated to whatever you set it to during the previous fix, which was merged here from the `main branch`
* The new fact you added in the branch for adding a new fact is unaffected and remains as it is

This scenario exemplifies how Git pinpoints exact lines of divergence between branches, helping you resolve merge conflicts with precision.

## So let's resolve the merge conflict!

1. Open the file containing the merge conflict in your favorite IDE or text editor

2. Look for the conflict markers `<<<<<<<`, `=======`, and `>>>>>>>`
  * The lines between `<<<<<<< HEAD` and `=======` show **your** changes in the `<your-name>-more-facts branch`
  * The lines between `=======` and `>>>>>>> <your-name>-more-facts` represent the changes from the `main branch` that are causing the conflict

3. Decide how you want to resolve the conflict: we want to keep the changes from the `<your-name>-more-facts branch`, so simply edit the file accordingly to keep the changes you want

4. Once you've resolved the conflict (edited the file and saved), go back to the terminal and stage, commit, and push the changes to the `<your-name>-more-facts branch` with a message saying *"Resolved merge conflict in \<filename\>"*

5. This marks the resolution of the merge conflict, and you can now proceed with merging the branches as intended, so: switch to the `main branch` and merge it with the `<your-name>-more-facts branch`

6. Remember to push the changes in the `main branch` after the merge

7. In your browser, navigate to the remote repository and verify your changes (both `main` and `<your-name>-more-facts` branches are updated)

## Great job, we are done! 🤩

Finally, clean up after yourself by deleting both the local and remote branches.