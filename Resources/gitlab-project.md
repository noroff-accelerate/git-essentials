# How to Create a Project in GitLab

New projects in GitLab are created using the web interface.

Unless otherwise specified, for the activities and tasks here, we will create **private**, **blank** projects, using our personal namespace.

[Sign into your account](https://about.gitlab.com/) and from your dashboard:

* In the top right corner, select **"New project"**, or in the top left corner, select the **“+”** sign and pick **"New project/repository".**
* GitLab presents you with various project templates to choose from. We’ll choose **"Create blank project"**.
* Fill in project details, including the project name and description, and choose visibility level **"Private"**.
  * Under **"Project URL"**, enter your GitLab username as the namespace for the project.
  * Under **"Project Configuration"**, make sure to untick **"Initialize repository with a README"** (we’ll push up an existing repository when needed).
* Press **"Create project"** at the bottom of the screen.