# Final Assignment - Project Management on GitLab

This assignment extends on the GitLab **Recipes** project we created in lesson 2. Please make sure you've completed [Lesson 2 Task](../Lesson%202%20-%20Collaborating%20with%20remote%20repositories/Task%20-%20Collaborating%20with%20remote%20repositories.md).

Our goal is to monitor daily sales in our restaurant and identify dishes with low sales. We'll use a Python script (provided below) to filter these from our daily sales data. By automating this process through a CI/CD pipeline, we'll generate regular reports, used to continuously adapt and improve our restaurant's offerings based on data-driven insights.

**Ensure you have someone to collaborate with on the project (if not, please notify the instructor!). This will act as your team** 🤜🤛

## Project setup

You are given the role as senior developer in charge of the team, and required to plan, manage, document, and do the work using GitLab's built in support for project management (hint: GitLab issues and boards).

- Create at least three issues, naming them accordingly for:
  - implementing the application
  - creating and configuring automated build/delivery (CI/CD)
  - documenting the project (hint: README.md)
- Assign issues to the team members, *one each* for implementing the application and creating the CI/CD pipeline, *none of you* for documenting the project (you will take turns working on this).
- Create the necessary labels to categorize your issues, such as "In progress", "Development", "Configuration" etc. Add as many labels as you believe is necessary and adjust the issue board to reflect the different states you want to track.
- Create a milestone, with a due date set at a reasonable time considering the amount of work and assign it to the issues (do some "off the top of your head" estimation).

## Workflow management

Decide on a workflow and branching strategy that will minimize the risk of delays and code merge conflict, while improving code/content stability and quality (hint: GitLab merge requests, GitHub Flow).

- Ensure both collaborators have cloned the repository and *create the necessary branches for your issues* following the GitHub Flow strategy. Don't forget to push the branches to the remote repository!
- Remember to update the corresponding issues as you progress through your work.

Do all the necessary work in the respective branches, and when done create a merge request for that branch with your teammate as the reviewer. The work to be done is:

- **Implement the application, adding the necessary Python script and sales data register file**
  - You are free to implement this any way you see fit, however a sample application is provided below. Feel free to use that.
  - Add the files to the root directory of the project; add, commit and push the changes to the remote repository with an appropriate message.

- **Create an automated process for generating a report, every time our sales register data is updated (hint: GitLab CI/CD).**
  - Create a CI/CD pipeline (`.gitlab-ci.yml`) that triggers only on changes to the main branch.
  - Add the pipeline configuration to the root directory of the project; add, commit and push it to the remote repository with an appropriate message.
  - The pipeline executes the application, reads the daily sales register file, and creates one artifact: the sales report (aptly named `poor_sales_[YYYY-MM-DD].csv` in the provided script).
  - The limit we use for determining if a dish has sold poorly must be configurable from the GitLab portal, so that we can change it without having to change any code or sales register data (hint: variables).

- **Document the project with a README, explaining core functionality, usage, and contributers**
  - Follow best pratices for documenting hosted Git repositories and share any relevant information about the project.

## Collaboration considerations

Since both of you are working the issue on documentation, you will either need to coordinate on the workflow regarding branching and merging, or "wing it" and manage any potential merge conflicts. Either way, make sure to properly update the corresponding issue as you work.

As the work concludes and you receive notification to review merge requests, review the changes in the request and take the appropriate action (hopefully approving). When done, merge the branch back to the main branch.

## Running the CI/CD pipeline

Once the CI/CD pipeline branch is merged to the main branch, it will trigger a run (if implemented correctly, i.e., no bugs).

Assuming the application implementation branch has been merged already, it will be executed as configured in the pipeline and generate an output file. You can download the output file from the project's GitLab portal under **Build > Pipelines**, by selecting the small download arrow on the right side of the pipeline row.

If the application implementation branch has *not* been merged prior to the CI/CD pipeline, the pipeline will fail. This is expected and no cause for concern. The pipeline will trigger again as soon as the application implementation branch has been merged to the main branch.

We are now done and anytime we want a new report on what's selling too poorly, we need only to update the sales data (`daily_sales.csv` if you've been using the assets provided here), commit and push the changes to the remote repository, and download the generated report.

The proper approach to this, given our workflow, would be to create issues assigned to team members for the days they are responsible to update the sales data. The changes should also be made in corresponding branches, reviewed by someone else prior to merging to main; regardless of how small they are.

_______________

Since our focus is on workflow, automation and project management in GitLab, a sample Python script that performs the necessary data filtering is provided:

```python
import csv
import sys
from datetime import datetime

def filter_low_sales(input_file_path, output_file_path, sales_limit=30):
    with open(input_file_path, "r") as infile, open(output_file_path, "w", newline="") as outfile:
        reader = csv.DictReader(infile)
        writer = csv.DictWriter(outfile, fieldnames=reader.fieldnames)
        writer.writeheader()

        for row in reader:
            if int(row['Sales']) < sales_limit:
                writer.writerow(row)

if __name__ == "__main__":
    current_date = datetime.now().strftime("%Y-%m-%d")
    report_file = f"poor_sales_{current_date}.csv"
    filter_low_sales("daily_sales.csv", report_file, int(sys.argv[1]))
```

The script assumes a file named `daily_sales.csv` located in the same directory as itself. This file contains the daily sales register for our restaurant and looks like this:

```csv
Dish,Sales
Salad,57
Hamburger,51
Pasta,24
BLT,18
```

**Feel free to use the provided Python script and csv sample data as they are.**

The script can be run with the command `python main.py 20`; the **"20"** at the end being the argument used for the limit set on what we would consider a poor sale. You'll need this for your pipeline configuration.