# Lesson 1 Task - Introduction to Version Control

In this task, you will create a new Git repository and make a series of commits to it, checking the status and log of your work as you progress, and revert changes to restore previous versions.

Imagine your Git repository as a container for the recepies of a restaurant. Let's start by creating the necessary folder and adding a few recipes. Make sure you have [Git installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) on your machine!

1. Create a new folder called **Recipes**

2. Inside the **Recipes** folder, initialize a new Git repository

3. Make a file called **salad.txt** 🥗 and another called **hamburger.txt** 🍔

4. Check the status of your repository: verify that the newly created files are *"Untracked files"*

5. Add both files to your repository and make a commit that includes them both, with a message saying *"Added new recipes"*

6. In the **salad.txt** file, add the following recipe:
    ```
    Lettuce
    Tomato
    Cucumber
    Olive oil
    ```

7. In the **hamburger.txt** file, add the following recipe:
    ```
    Ground beef patty
    Hamburger bun
    Lettuce
    Tomato
    Ketchup
    ```

8. Make a new commit, including *only* the changes from **salad.txt**, with a commit message saying *"Added ingredients for a salad"*

9. Make a second commit including *only* the changes from **hamburger.txt**, with a commit message saying *"Added ingredients for a hamburger"*

10. Check the status of your repository: verify there are no pending changes (*"nothing to commit, working tree clean"*) and only 3 log entries reflecting the changes done so far

11. Change both files, adding a few new ingredients to enhance the recipes

**salad.txt**:
    
    Lettuce
    Tomato
    Cucumber
    Olive oil
    Avocado
    Croutons

**hamburger.txt**:

    Ground beef patty
    Hamburger bun
    Lettuce
    Tomato
    Ketchup
    Cheese
    Onion

12. Use Git to check and review modifications to the files: verify the new ingredients are visible as changes made

13. Check the status of your repository: verify the new changes as *"Changes not staged for commit"* and still only 3 log entries (we haven't commited any changes since the last time we checked!)

14. Stage and commit both files with a **single operation**, with a commit message saying *"Recepies updated with new ingredients"*

15. Check the status of your repository: verify there are no pending changes (*"nothing to commit, working tree clean"*) and 4 log entries reflecting all changes done so far, including the new entry reflecting our latest commit

16. Change **hamburger.txt** only, add a bad ingredient *"Expired mayonnaise"* and commit the change with a message saying *"This will cause food poisoning"*

17. Check the status of your repository: verify there are no pending changes (*"nothing to commit, working tree clean"*) and 5 log entries reflecting all changes done so far, including the new entry reflecting our mishap 🤮

18. Since we don't want our customers to get food poisoning, we'd better remove that last commit! Remove the previous commit completly from you repository and it's history. **No one must know!**

19. Check the status of your repository: verify there are no pending changes and **no log entry** indicating we messed up and caused food poisoning.

Well done, we have our recipes ready and have removed all trace of our contaminated hamburger recipe 🥳

**Optional**

Perform the *"Expired mayonnaise"* exercise again, but this time with the salad recipe. Stage and commit the bad ingredient to the salad recipe and then immediately realize your decision was regrettable. Given that we're now **obligated by law** to keep a record of all changes, regardless of any alterations to the recipe later, use the appropriate Git command to undo your last commit while maintaining a history of all changes.

What does the Git history log show now?